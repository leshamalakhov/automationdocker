import io.github.bonigarcia.wdm.WebDriverManager;
import io.github.cdimascio.dotenv.Dotenv;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.util.concurrent.TimeUnit;

public class SetupTest {
    public static WebDriver driver;

    @BeforeMethod
    public void setupMain() throws InterruptedException {
        Dotenv dotenv = Dotenv.load();
        String apiURLS = dotenv.get("TEST_API_URLS");
        int apiPort = Integer.parseInt(dotenv.get("TEST_API_PORT"));
        ChromeOptions handlingSSL = new ChromeOptions().setHeadless(true);
        handlingSSL.setAcceptInsecureCerts(true);
        handlingSSL.addArguments("--no-sandbox"); //Because start Driver on CI without window
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver(handlingSSL);
        driver.get("http://"+ apiURLS + ":" + apiPort);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().setSize(new Dimension(1024, 768)); //window size
        Thread.sleep(1000);
    }
    @AfterMethod
    public void closeDriver(){
        driver.quit();
    }
}
