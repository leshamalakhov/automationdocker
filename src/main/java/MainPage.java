import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class MainPage {

    WebDriver driver;
    public MainPage(WebDriver driver) {
        this.driver = driver;
    }

        private List<WebElement> listFilmName(){
            return driver.findElements(By.xpath("//p[contains(@class,'MuiTypography-root jss')]"));
        }
        public String listFileNameTXT(int value){
        return listFilmName().get(value).getText();
        }
        public void listFileNameClickOnFilm(int value){
         listFilmName().get(value).click();
        }

}
