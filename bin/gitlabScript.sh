#!/usr/bin/env bash

docker login -u gitlab-ci-token -p "$CI_JOB_TOKEN" "$CI_REGISTRY"
docker build . -t gitlab.com:leshamalakhov/automationdocker
docker push gitlab.com:leshamalakhov/automationdocker
