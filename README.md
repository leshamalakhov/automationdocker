#  Automation with gitlab CI and Docker-compose



### 1. .env file

| Variable  | Type   | Default        | Example     | Description               |
|-----------|--------|----------------|-------------|---------------------------|
| TEST_API_URLS | string | admin          | localhost   | Api Urls for test         |
TEST_PAGE|     string      | "*.java"       | "TestingFile" | for run one test or more  |
TEST_API_PORT|int  | 3000| 3000|Port for test|

### 2. Directory bin 
This directory created for shell script 
    
    1) dbScript.sh for migrate
    2) gtilabScript.sh for autorizate on gitlab

### 3. Directory etc
In this directory I saved the db

### 4. Directory src
        1) Main used for some class
        2) test used for some test

### 5. Start local project 
In my project I created some script for pickup a project and start to testng.
Or you can run the project this way, but if you already have a ready-made image of your tests (CI/CD was already build) : 

        1) docker-compose up -d app // for start project
        2) /bin/dbScript.sh //for migrate db 
        3) docker-compose run --rm automation //for  run test

